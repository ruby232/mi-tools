# Mi Tools

## Problem
This project is to solve the problem of Pixel 5 and the Mi Band 8 Pro. With this combination, notifications for incoming WhatsApp calls are not received.


## Solution
To solve this, a service is created that captures the notifications, filters the WhatsApp package and issues a new notification.

## ToDo
* Send a direct message to the Band and show it as an incoming call.
* Hang up from the Band

## Mi Band from Android
https://github.com/dkhmelenko/miband-android/tree/master/miband-sdk-kotlin
https://androidcontrol.blogspot.com/2016/04/android-control-ble-bluetooth-low.html
