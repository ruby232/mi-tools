package com.onebyte.mitools.service;

import android.util.Log;

import android.content.Intent;
import android.os.IBinder;
import android.service.notification.StatusBarNotification;
import android.service.notification.NotificationListenerService;

import java.util.Objects;


public class WhatsappNotificationListenerService extends NotificationListenerService {

    private static final String TAG = "WhatsappNotificationListenerService";
    private static final String CALL_NOTIFICATION_GROUP = "call_notification_group";

    /*
        These are the package names of the apps. for which we want to
        listen the notifications
     */
    private static final class ApplicationPackageNames {
        public static final String WHATSAPP_PACK_NAME = "com.whatsapp";
    }

    /*
        These are the return codes we use in the method which intercepts
        the notifications, to decide whether we should do something or not
     */
    public static final class InterceptedNotificationCode {
        public static final int OTHER_NOTIFICATIONS_CODE = 1; // We ignore all notification with code == 1

        public static final int WHATSAPP_CODE = 2;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        int notificationCode = matchNotificationCode(sbn);

        if (notificationCode != InterceptedNotificationCode.OTHER_NOTIFICATIONS_CODE) {
            Intent intent = new Intent("com.onebyte.mitools");
            intent.putExtra("code", notificationCode);
            sendBroadcast(intent);
        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        int notificationCode = matchNotificationCode(sbn);

        if (notificationCode == InterceptedNotificationCode.OTHER_NOTIFICATIONS_CODE) {
            return;
        }

        StatusBarNotification[] activeNotifications = this.getActiveNotifications();
        if (activeNotifications == null) {
            return;
        }

        for (StatusBarNotification activeNotification : activeNotifications) {
            if (notificationCode == matchNotificationCode(activeNotification)) {
                Intent intent = new Intent("com.onebyte.mitools");
                intent.putExtra("code", notificationCode);
                sendBroadcast(intent);
                break;
            }
        }
    }

    private int matchNotificationCode(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        String group = sbn.getNotification().getGroup();
        String channelId = sbn.getNotification().getChannelId();
        // Las llamadas de Whatsapp envian voip_notification_7 pero por ahora no es necesario

        Log.d(TAG, "Notification group:" + group);
        Log.d(TAG, "Notification channelId:" + channelId);

        if (packageName.equals(ApplicationPackageNames.WHATSAPP_PACK_NAME) && group != null && group.equals(CALL_NOTIFICATION_GROUP)) {
            return (InterceptedNotificationCode.WHATSAPP_CODE);
        }

        return (InterceptedNotificationCode.OTHER_NOTIFICATIONS_CODE);
    }
}