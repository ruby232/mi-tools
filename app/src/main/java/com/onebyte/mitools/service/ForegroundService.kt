package com.onebyte.mitools.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.onebyte.mitools.R
import java.lang.UnsupportedOperationException

class ForegroundService: Service() {

    companion object {
        private const val ONGOING_NOTIFICATION_ID = 101
        private const val CHANNEL_ID = "1001"

        fun startService(context: Context) {
            val intent = Intent(context, ForegroundService::class.java)
            context.stopService(intent)
            context.startForegroundService(intent)
        }

        fun stopService(context: Context) {
            val intent = Intent(context, ForegroundService::class.java)
            context.stopService(intent)
        }
    }

    private lateinit var notificationManager: NotificationManager

    private var isStarted = false
    private lateinit var notifyBroadcastReceiver: IncomeNotifyBroadcastReceiver
    private lateinit var notification: Notification

    override fun onCreate() {
        super.onCreate()
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notifyBroadcastReceiver = IncomeNotifyBroadcastReceiver()
        val intentFilter = IntentFilter()
        intentFilter.addAction("com.onebyte.mitools")
        registerReceiver(notifyBroadcastReceiver, intentFilter, RECEIVER_EXPORTED)
    }

    override fun onDestroy() {
        super.onDestroy()
        isStarted = false
        unregisterReceiver(notifyBroadcastReceiver)
    }

    override fun onBind(p0: Intent?): IBinder? {
        throw UnsupportedOperationException()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (!isStarted) {
            makeForeground()
            isStarted = true
        }
        return START_NOT_STICKY
    }

    private fun makeForeground() {
        createServiceNotificationChannel()

        notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Mi Tools")
            .setContentText("Listen WhatsApp calls notifications ...")
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .build()

        startForeground(ONGOING_NOTIFICATION_ID, notification)
    }

    private fun createServiceNotificationChannel() {
        val channel = NotificationChannel(
            CHANNEL_ID,
            "Mi Tools Channel",
            NotificationManager.IMPORTANCE_DEFAULT
        )

        notificationManager.createNotificationChannel(channel)
    }


    /**
     *
     */
    class IncomeNotifyBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val receivedNotificationCode = intent.getIntExtra("code", -1)
            println("Notification Code IncomeNotifyBroadcastReceiver:$receivedNotificationCode")
            val miBandServiceIntent = Intent(context, MiBandService::class.java)
            context.startService(miBandServiceIntent)
        }
    }
}